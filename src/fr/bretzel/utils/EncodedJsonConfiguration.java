package fr.bretzel.utils;

import com.google.common.io.Files;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;

/**
 * This class allows for creating a JsonConfiguration that saves files using a specified file encoding.
 */

class EncodedJsonConfiguration extends JsonConfiguration {

    private final Charset charset;

    EncodedJsonConfiguration(final Charset charset) throws UnsupportedEncodingException {
        this.charset = charset;
    }
    EncodedJsonConfiguration(final String charset) throws UnsupportedEncodingException, IllegalCharsetNameException {
        this(Charset.forName(charset));
    }
    @Override
    public void save(final File file) throws IOException {
        Files.createParentDirs(file);
        final String data = saveToString();
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), charset));
            writer.write(data);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
