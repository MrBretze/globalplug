package fr.bretzel.permission;

import com.google.common.collect.Sets;

import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by MrBretze on 20/01/2015.
 */

public class Group {

    private String groupName = "";
    private boolean defaut = true;
    private Set<String> getPermission = Sets.newHashSet();
    private Set<UUID> players = Sets.newConcurrentHashSet();

    public Group(Set<String> permission, String groupName) {
       setGroupName(groupName);
        getPermission().clear();

        for(String s : permission) {
            addPermission(s);
        }
    }

    public Group(List<String> permission, String groupName) {
        setGroupName(groupName);
        getPermission().clear();

        for(String s : permission) {
            addPermission(s);
        }
    }

    public void setDefaut(boolean defaut) {
        this.defaut = defaut;
    }

    public boolean isDefaut() {
        return this.defaut;
    }

    public boolean contains(Player player) {
        for(UUID p : getPlayers()) {
            if(p.equals(player.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    public void addPlayer(Player player) {
        getPlayers().add(player.getUniqueId());
    }

    public Set<String> getUUIDS() {
        Set<String> list = new HashSet<String>();
        while (getPlayers().iterator().hasNext()) {
            String uuid = getPlayers().iterator().next().toString();
            if(!list.contains(uuid)) {
                list.add(uuid);
            }
        }
        return list;
    }

    public Set<UUID> getPlayers() {
        return this.players;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void addPermission(String permission) {
        getPermission().add(permission);
    }

    public void addPermission(List<String> permission) {
        while (permission.iterator().hasNext()) {
            String perm = permission.iterator().next();
            addPermission(perm);
        }
    }

    public void removePermission(String permission) {
        getPermission().remove(permission);
    }

    public Set<String> getPermission() {
        return this.getPermission;
    }
}
