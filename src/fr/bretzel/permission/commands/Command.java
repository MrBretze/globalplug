package fr.bretzel.permission.commands;

import com.google.common.collect.ImmutableList;

import fr.bretzel.main.Main;

import fr.bretzel.utils.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by MrBretze on 22/01/2015.
 */

public class Command implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String label, String[] args) {
        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if(player.hasPermission("globalplug.commands.permission")) {
                if(args.length > 0) {
                    if(args[0].equalsIgnoreCase("Players")) {
                        if(args.length > 1) {
                            if(args[1].equalsIgnoreCase("Info")) {
                                if(args.length > 2) {
                                    if(Bukkit.getPlayer(args[2]) != null) {
                                        Player selectPlayer = Bukkit.getPlayer(args[2]);
                                        player.sendMessage(ChatColor.GREEN + selectPlayer.getName() + " group is: " + ChatColor.AQUA + Main.getPermission().getGroupByPlayer(selectPlayer).getGroupName());
                                        player.sendMessage(ChatColor.GREEN + selectPlayer.getName() + " permission: ");
                                        for(String s : Main.getPermission().getGroupByPlayer(selectPlayer).getPermission()) {
                                            player.sendMessage(ChatColor.GREEN + "- " + ChatColor.AQUA + s);
                                        }
                                        return true;
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Player not found !");
                                        return true;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Your group is: " + ChatColor.AQUA + Main.getPermission().getGroupByPlayer(player).getGroupName());
                                    player.sendMessage(ChatColor.GREEN + "Your permission: ");
                                    for(String s : Main.getPermission().getGroupByPlayer(player).getPermission()) {
                                        player.sendMessage(ChatColor.GREEN + "- " + ChatColor.AQUA + s);
                                    }
                                    return true;
                                }
                            } else if(args[1].equalsIgnoreCase("Permission")) {
                                if(args.length > 2) {
                                    if(args[2].equalsIgnoreCase("add")) {
                                        if(args.length > 3) {

                                        } else {
                                            player.sendMessage(ChatColor.RED + "Usage: /perm Players Permission add <Player>");
                                            return true;
                                        }
                                    } else if(args[2].equalsIgnoreCase("remove")) {
                                        if(args.length > 3) {
                                            if(Bukkit.getPlayer(args[3]) != null) {
                                                if(args.length > 4) {
                                                    Player userPlayer = Bukkit.getPlayer(args[3]);
                                                    User user = new User(userPlayer);
                                                    user.getPermissionSet().add(args[4]);
                                                } else {
                                                    player.sendMessage(ChatColor.RED + "Usage: /perm Players Permission remove <Player> <Permission>");
                                                    return true;
                                                }
                                            } else {
                                                player.sendMessage(ChatColor.RED + "Player not found !");
                                                return true;
                                            }
                                        } else {
                                            player.sendMessage(ChatColor.RED + "Usage: /perm Players Permission remove <Player> <Permission>");
                                            return true;
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Usage: /perm Players Permission < add | remove >");
                                        return true;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Usage: /perm Players Permission < add | remove >");
                                    return true;
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Usage: /perm Players <Info | Permission>");
                                return true;
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Usage: /perm Players <Info | Permission>");
                            return true;
                        }
                    } else if(args[0].equalsIgnoreCase("Group")) {
                        if(args.length > 1) {
                            //TODO: Je sais pas quoi mettre
                            player.sendMessage(ChatColor.RED + "Usage: /perm group <??????>");
                            return true;
                        } else {
                            player.sendMessage(ChatColor.RED + "Usage: /perm group <??????>");
                            return true;
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Usage: /perm <Players | Group>");
                        return true;
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Usage: /perm <Players | Group>");
                    return true;
                }
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
        return ImmutableList.of();
    }
}
