package fr.bretzel.main;

import com.google.common.collect.Lists;

import fr.bretzel.permission.Group;
import fr.bretzel.permission.Permission;
import fr.bretzel.permission.commands.Command;
import fr.bretzel.utils.JsonConfiguration;

import fr.bretzel.utils.User;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by MrBretze on 20/01/2015.
 */

public class Main  extends JavaPlugin {

    private volatile static JsonConfiguration permConfiguration = null;
    private volatile static File permFile = null;

    private volatile static Permission p = new Permission();

    private volatile static Main instance;

    private volatile static Collection<User> users = new LinkedList<User>();

    @Override
    public void onEnable() {
        instance = this;

        enablePermission();

        getPermission().init(this);
    }

    @Override
    public void onDisable() {
        getPermission().disable();
    }

    public static File getPermissionFile() {
        return permFile;
    }
    public static JsonConfiguration getPermissionConfig() {
        return permConfiguration;
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        if(label.equalsIgnoreCase("test")) {
            sender.sendMessage(((Player) sender).getUniqueId().toString());
            return true;
        }
        return false;
    }

    public static void savePermission() {
        try {
            getPermissionConfig().save(getPermissionFile());
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    public static Permission getPermission() {
        return p;
    }

    public void enablePermission() {

        getCommand("perm").setExecutor(new Command());
        getCommand("permission").setExecutor(new Command());
        getCommand("permission").setTabCompleter(new Command());
        getCommand("perm").setTabCompleter(new Command());

        permFile = new File(getDataFolder(), "permission.json");

        if(!permFile.exists()) {
            try {
                new File(getDataFolder().toString()).mkdir();
                permFile.createNewFile();
            } catch (IOException e) {}
        }

        permConfiguration = JsonConfiguration.loadConfiguration(permFile);

        if(!getPermissionConfig().isSet("groups")) {
            getPermissionConfig().set("groups.default.permission", new ArrayList<String>());
            getPermissionConfig().set("groups.default.default", true);

            List<String> perm = Lists.newArrayList();
            perm.add("bukkit.command.*");
            perm.add("b.test.lol");

            getPermissionConfig().set("groups.admin.permission", perm);
            getPermissionConfig().set("groups.admin.default", false);
        }

        ConfigurationSection section = getPermissionConfig().getConfigurationSection("groups");

        for(String gName : section.getKeys(false)) {
            getPermission().addGroup(gName, section.getStringList(gName + ".permission"));
            getPermission().getGroupByName(gName).setDefaut(section.getBoolean(gName + ".default"));
        }

        for(Group g : Main.getPermission().getGroupList()) {
            if(g.isDefaut()) {
                getPermission().setDefaultGroup(g);
            }
        }
        savePermission();
    }

    public static Main getInstance() {
        return instance;
    }

    public static Collection<User> getUsers() {
        return users;
    }

    public static User getUserByPlayer(Player player) {
        Iterator<User> userIterator = getUsers().iterator();
        while (userIterator.hasNext()) {
            User u = userIterator.next();
            if(u.getUniqueId().toString().equals(player.getUniqueId().toString())) {
                return u;
            }
        }
        return new User(player);
    }

    public static User getUserByPlayer(String name) {
        Iterator<User> userIterator = getUsers().iterator();
        while (userIterator.hasNext()) {
            User u = userIterator.next();
            if(u.getPlayer().getName().equals(name)) {

            }
        }
        if(Bukkit.getPlayer(name) != null) {
            return new User(Bukkit.getPlayer(name));
        }
        return new User(Bukkit.getOfflinePlayer(name));
    }
}
