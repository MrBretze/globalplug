package fr.bretzel.permission;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import fr.bretzel.main.Main;
import fr.bretzel.permission.event.PermissionListener;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.PluginManager;

import java.util.*;

/**
 * Created by MrBretze on 20/01/2015.
 */

public class Permission {

    private Map<Group, Set<String>> getPermissions = Maps.newConcurrentMap();
    private HashMap<UUID, PermissionAttachment> permissionAttachment = Maps.newHashMap();
    private Map<Group, UUID> getPlayers = Maps.newConcurrentMap();
    private List<Group> groupList = Lists.newLinkedList();
    private Group defaultGroup = null;


    public Group getGroupByPlayer(Player player) {
        Iterator<Group> groupIterator = getGroupList().iterator();
        while (groupIterator.hasNext()) {
            Iterator<UUID> uuidIterator = groupIterator.next().getPlayers().iterator();
            if(player.getUniqueId().toString().equalsIgnoreCase(player.getUniqueId().toString())) {
                return groupIterator.next();
            }
        }
        return null;
    }
    public Group getGroupByName(String name) {
        Iterator<Group> groupIterator = getGroupList().iterator();
        while (groupIterator.hasNext()) {
            if(groupIterator.next().getGroupName().equals(name)) {
                return groupIterator.next();
            }
        }
        return null;
    }

    public void addGroup(String groupName, Set<String> permission) {
        Group g = new Group(permission, groupName);
        groupList.add(g);
    }

    public void addGroup(String groupName, List<String> permission) {
        Group g = new Group(permission, groupName);
        groupList.add(g);
    }

    public void addPermissionForGroup(Group g, String permission) {
        g.addPermission(permission);
    }


    public void addPermissionForGroup(String group, String permission) throws NullPointerException {
        getGroupByName(group).addPermission(permission);
    }

    public void addPermissionForPlayer(Player player) {

    }

    public Map<UUID, PermissionAttachment> getPermissionAttachment() {
        return permissionAttachment;
    }

    public List<Group> getGroupList() {
        return this.groupList;
    }

    public Group getDefaultGroup() {
        return defaultGroup;
    }

    public void setDefaultGroup(Group defaultGroup) {
        this.defaultGroup = defaultGroup;
    }

    public void init(Main main) {
        Server server = main.getServer();
        PluginManager manager = server.getPluginManager();
        manager.registerEvents(new PermissionListener(main), main);
    }

    public void disable() {
        for(Group group : getGroupList()) {
            Main.getPermissionConfig();
            Main.savePermission();
        }
    }
}
