package fr.bretzel.utils;

import fr.bretzel.main.Main;
import fr.bretzel.permission.Group;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Loic on 31/01/2015.
 */

public class User {

    private File file;
    private JsonConfiguration configuration;
    private Group group;
    private Player player;
    private OfflinePlayer offlinePlayer;
    private Set<String> permissionSet = new LinkedHashSet<String>();

    public User(Player player) {
        setPlayer(player);
        File f = new File(Main.getInstance().getDataFolder() + File.separator + "players" + File.separator);
        f.mkdir();
        setFile(new File(f, getPlayer().getUniqueId() + ".json"));
        if(!getFile().exists()) {
            try {
                getFile().createNewFile();
            } catch (IOException e) {}
        }
        configuration = JsonConfiguration.loadConfiguration(getFile());
    }

    public User(OfflinePlayer player) {
        setPlayer(player.getPlayer());
        File f = new File(Main.getInstance().getDataFolder() + File.separator + "players" + File.separator);
        f.mkdir();
        setFile(new File(f, player.getUniqueId() + ".json"));
        if(!getFile().exists()) {
            try {
                getFile().createNewFile();
            } catch (IOException e) {}
        }
        configuration = JsonConfiguration.loadConfiguration(getFile());
    }

    public void setConfiguration(JsonConfiguration configuration) {
        this.configuration = configuration;
    }

    public JsonConfiguration getConfiguration() {
        return configuration;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public Collection<String> getPermissionSet() {
        return permissionSet;
    }

    public UUID getUniqueId() {
        return getPlayer().getUniqueId();
    }

    public User saveUser() {
        try {
            getConfiguration().save(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this;
    }

    public User reloadUser() {
        setPlayer(Bukkit.getPlayer(getConfiguration().getUniqueId("player.uuid")));
     return this;
    }
}
