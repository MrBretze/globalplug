package fr.bretzel.permission.event;

import fr.bretzel.main.Main;
import fr.bretzel.permission.Group;

import fr.bretzel.permission.Permission;
import fr.bretzel.utils.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MrBretze on 20/01/2015.
 */

public class PermissionListener implements Listener {

    public Main main = null;

    public PermissionListener(Main main) {
        this.main = main;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.getEffectivePermissions().clear();
        PermissionAttachment attachment = player.addAttachment(main);

        User user = new User(player);

        if(user.getGroup() == null) {
            Main.getPermission().getDefaultGroup().addPlayer(user.getPlayer());
        }

        player.recalculatePermissions();
        Main.getPermission().getPermissionAttachment().put(player.getUniqueId(), attachment);
    }

    @EventHandler
    public void onPlayerBreak(BlockBreakEvent event) {
        if(event.getPlayer().hasPermission("b.test.lol")) {
            event.getPlayer().sendMessage("Tu a la permission !!!!!!!!!!");
            Bukkit.broadcastMessage(Main.getPermission().getGroupByPlayer(event.getPlayer()).getGroupName());
            Bukkit.broadcastMessage(Main.getPermission().getPermissionAttachment().get(event.getPlayer().getUniqueId()).getPermissions().toString());
            Bukkit.broadcastMessage("Normalement il y a sa " + Main.getPermission().getGroupByPlayer(event.getPlayer()).getPermission().toString());
        } else {
            event.getPlayer().sendMessage("Tu a pas la permission");
            event.setCancelled(true);
            Bukkit.broadcastMessage(Main.getPermission().getGroupByPlayer(event.getPlayer()).getGroupName());
            Bukkit.broadcastMessage(Main.getPermission().getPermissionAttachment().get(event.getPlayer().getUniqueId()).getPermissions().toString());
            Bukkit.broadcastMessage("Normalement il y a sa " + Main.getPermission().getGroupByPlayer(event.getPlayer()).getPermission().toString());
        }
    }
}
