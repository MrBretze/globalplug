package fr.bretzel.utils;

import org.bukkit.configuration.file.FileConfigurationOptions;

/**
 * Mandatory configuration options class for JsonConfiguration.
 */

public class JsonConfigurationOptions extends FileConfigurationOptions {

    protected JsonConfigurationOptions(final JsonConfiguration configuration) {
        super(configuration);
    }
    @Override
    public JsonConfiguration configuration() {
        return (JsonConfiguration) super.configuration();
    }
    @Override
    public JsonConfigurationOptions copyDefaults(final boolean value) {
        super.copyDefaults(value);
        return this;
    }
    @Override
    public JsonConfigurationOptions pathSeparator(final char value) {
        super.pathSeparator(value);
        return this;
    }
    @Override
    public JsonConfigurationOptions header(final String value) {
        super.header(value);
        return this;
    }
    @Override
    public JsonConfigurationOptions copyHeader(final boolean value) {
        super.copyHeader(value);
        return this;
    }
}
